/*
 * Algebra labs.
 */
 
package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.demo.persistence.InMemoryItemRepository;
import com.example.demo.persistence.ItemRepository;

// Declare as a configuration class
@Configuration
public class SpringRepositoryConfig {
	
	// TODO: Inject the environment
	
	
	// Declare the item repository bean
	@Bean
	public ItemRepository itemRepository() {
		// Pass a CatalogData to the repository
		InMemoryItemRepository rep = new InMemoryItemRepository();

		// TODO: Retrieve the maxSearchResults property from the environment (as an Integer)
		// TODO: Initialize the maxSearchResults property or rep using it

		return rep;
	}
}