/*
 * Algebra labs.
 */
 
package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.demo.persistence.ItemRepository;
import com.example.demo.service.Catalog;
import com.example.demo.service.CatalogImpl;

// Declare as a configuration class
@Configuration
public class SpringServicesConfig {
	
	// TODO: Inject the repository
	ItemRepository repository;

	// Declare the catalog bean definition
	@Bean
	public Catalog catalog() {

		// TODO: Create the CatalogImpl, passing in the repository
		CatalogImpl catalog = null;
		
		return catalog;
	}

}